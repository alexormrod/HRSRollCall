var app;

function init(){

	var data = [
		{ id: 0, FirstNames: "Thara", Surname: "Shetty", Contractor: "Human Recognition Systems", DOB: "08/08/2018" },
		{ id: 1, FirstNames: "Alex", Surname: "Ormrod", Contractor: "Carillion", DOB: "08/05/2014" },
		{ id: 2, FirstNames: "Eoghan", Surname: "Brady", Contractor: "Interserve", DOB: "03/08/1995" },
		{ id: 3, FirstNames: "Phil", Surname: "Smith", Contractor:"Interserve", DOB: "12/02/1999" },
		{ id: 4, FirstNames: "Adam", Surname: "Smith", Contractor: "Human Recognition Systems", DOB: "13/08/1980" },
		{ id: 5, FirstNames: "Sean", Surname: "Devine", Contractor: "Human Recognition Systems", DOB: "08/08/1980" },
		{ id: 6, FirstNames: "Mike", Surname: "Duffy", Contractor: "Human Recognition Systems", DOB: "20/08/1997" },
		{ id: 7, FirstNames: "Charlotte", Surname: "Smith", Contractor: "Galliford Try", DOB: "08/01/1991" },
		{ id: 8, FirstNames: "Ion", Surname: "CapriSun", Contractor:"Galliford Try", DOB: "04/08/1991" },
		{ id: 9, FirstNames: "Chris", Surname: "Parslow", Contractor: "Human Recognition Systems", DOB: "7/08/1995" },
		{ id: 10, FirstNames: "Leon", Surname: "Pennington", Contractor: "Morgan Sindall", DOB: "08/01/1997" },
		{ id: 11, FirstNames: "Michael", Surname: "James", Contractor: "Morgan Sindall", DOB: "12/08/1997" },
		{ id: 12, FirstNames: "Sean", Surname: "Kelly", Contractor: "Morgan Sindall", DOB: "11/08/2018" },
		{ id: 13, FirstNames: "Ste", Surname: "Collins", Contractor: "Morgan Sindall", DOB: "22/01/1997" },
		{ id: 14, FirstNames: "Jim", Surname: "Carey", Contractor: "Morgan Sindall", DOB: "23/04/1995" },
		{ id: 15, FirstNames: "Tom", Surname: "Smith", Contractor: "Galliford Try", DOB: "14/01/2018" },
		{ id: 16, FirstNames: "Jimmy", Surname: "Carr", Contractor:"Galliford Try", DOB: "11/08/2018" },
		{ id: 17, FirstNames: "Steven The Devil", Surname: "French", Contractor: "Human Recognition Systems", DOB: "07/08/1995" },
		{ id: 18, FirstNames: "Daniel", Surname: "Craig", Contractor: "Human Recognition Systems", DOB: "09/04/2018" },
		{ id: 19, FirstNames: "Tim", Surname: "Dalton", Contractor: "Morgan Sindall", DOB: "01/08/1991" },
		{ id: 20, FirstNames: "Frank", Surname: "Bruno", Contractor: "Human Recognition Systems", DOB: "02/04/1997" },
		{ id: 21, FirstNames: "Mary", Surname: "Poppins", Contractor: "Morgan Sindall", DOB: "08/07/1991" },
		{ id: 22, FirstNames: "Bruno", Surname: "Mars", Contractor: "Human Recognition Systems", DOB: "04/07/2018" },
		{ id: 23, FirstNames: "Leon", Surname: "The Professional", Contractor: "Morgan Sindall", DOB: "08/01/1997" },
		{ id: 24, FirstNames: "Tony", Surname: "Stark", Contractor: "Morgan Sindall", DOB: "12/08/1997" },
		{ id: 25, FirstNames: "Bruce", Surname: "Wayne", Contractor: "Morgan Sindall", DOB: "11/08/2018" },
		{ id: 26, FirstNames: "Mike", Surname: "Tyson", Contractor:"Galliford Try", DOB: "11/08/2018" },
		{ id: 27, FirstNames: "Danny", Surname: "Dyer", Contractor: "Human Recognition Systems", DOB: "07/08/1995" },
		{ id: 28, FirstNames: "Michael", Surname: "Moore", Contractor: "Human Recognition Systems", DOB: "09/04/2018" },
		{ id: 29, FirstNames: "Steven", Surname: "Gerrard", Contractor: "Morgan Sindall", DOB: "01/08/1991" },
		{ id: 30, FirstNames: "Luis", Surname: "Suarez", Contractor: "Human Recognition Systems", DOB: "02/04/1997" },
		{ id: 31, FirstNames: "Djimi", Surname: "Traore", Contractor: "Morgan Sindall", DOB: "08/07/1991" },
		{ id: 32, FirstNames: "Frankie", Surname: "Boyle", Contractor: "Human Recognition Systems", DOB: "04/07/2018" },
	];

	data.sort(function(a, b){
		if (a.Surname < b.Surname)
			return -1;
		if (a.Surname > b.Surname)
			return 1;
		if (a.FirstNames < b.FirstNames)
			return -1;
		if (a.FirstNames > b.FirstNames)
			return 1;

		return 0;
	});

	Vue.component('employee-item', {
		props: ['employee', 'index'],
		methods: {
			toggleEmployee: function(index, event){

				var key = event.target.getAttribute('data-key');
				var isChecked = event.target.checked;

				this.$forceUpdate();

				for(var i = 0; i < data.length; i++){
					if(data[i].id == key){
						data[i].IsChecked = isChecked;
						break;
					}
				}

				isChecked ? app.selectedEmployees++ :	app.selectedEmployees--;
			}
		},
		template:
		`<label :class="['row no-gutters border-left-7 employee-label', index % 2 == 0 ? 'bg-0' : 'bg-1', employee.Hide ? 'hidden' : '', employee.IsChecked ? 'border-success' : 'border-light']">
			<div class="col-auto pl-0 p-3">
			<img src="images/no-avatar.jpg" class="avatar-img">
			</div>
			<div class="col pr-3 pt-3 pb-2point5 pr-3">
			<h6 class="big mb-1 text-uppercase">{{ employee.Surname }}, {{ employee.FirstNames }}  {{ employee.Hide }}</h6>
			<div>{{ employee.DOB }}</div>
			<div>{{ employee.Contractor }}</div>
			</div>
			<div class="form-switcher form-switcher-lg pl-1 pt-3 pr-3 mt-2 float-right">
			<input type="checkbox" v-model="employee.IsChecked" :checked="employee.IsChecked" :data-key="employee.id" v-on:click="toggleEmployee(index, $event)" :name="'switcher-name' +  index"   :id="'switcher-id-' + index">
			<label class="switcher" :for="'switcher-id-' + index "></label>
			</div>
		</label>`
	});


	Vue.component('no-results', {
		template:
		`<div class="row no-gutters bg-0">
		<div class="col p-3">
		No Results
		</div>
		</div>`
	});


	app = new Vue({
		el: '#app',
		methods: {
			scrollToTop: function(){
				document.querySelector("html").scrollTop = 0;
				document.querySelector("body").scrollTop = 0;
			},
			seachEmployeesKeyDown: function(){
				if(app.searchValue.length == 0){
					app.currentYScrollPos = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
				}
			},
			searchEmployeesClear: function(){
					app.searchValue = '';
					app.searchEmployees();
			},
			scrollToSavedPosition: function(){
				window.scrollTo(0, app.currentYScrollPos);
			},
			searchEmployees: function(){

				this.employees = data.filter(function(emp){

					var searchValues = app.searchValue.toUpperCase().split(" ");

					for(var i = 0; i < searchValues.length; i++){

						var searchVal = searchValues[i];
						var match = (emp.Contractor != null && emp.Contractor.toUpperCase().indexOf(searchVal) > -1) ||
						(emp.FirstNames != null && emp.FirstNames.toUpperCase().indexOf(searchVal) > -1) ||
						(emp.Surname != null && emp.Surname.toUpperCase().indexOf(searchVal) > -1);

						if(match == false) return false;
					}

					return true;
				});

				// after finished rendering view scroll back to original position
				app.$nextTick(function(){
					if(app.searchValue.length == 0){
						app.scrollToSavedPosition();
					}
				});
			},
		},
		data: {
			selectedEmployees: 0,
			searchValue: '',
			employees: data,
			employeeCount: data.length,
			currentYScrollPos: 0
		}
	});
}


window.addEventListener('load', init);
